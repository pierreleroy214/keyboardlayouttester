const text = "Gaal Dornick, car tel était son nom, n'était encore qu'un jeune homme fraîchement débarqué de sa planète natale. Un provincial qui n'avait même jamais vu Trantor. Du moins, pas de ses propres yeux. Bien des fois, par contre, il avait eu l'occasion de contempler la planète à l'hypervidéo. D'autres fois, un peu moins souvent, en regardant les actualités en tridi, fasciné par le formidable impact visuel des ces images, il avait assisté à un couronnement impérial ou à l'ouverture d'un concile galactique. Bien qu'il eût passé jusque-là toute existence sur Syntax, une planète en orbit autour d'une étoile située aux confins de la Nébuleuse bleue, Gaal Dornick n'était pas totalement coupé du reste de la civilisation. Ce qu'il vous faut comprendre, c'est qu'à cette époque-là, dans la galaxie, nulle planète ne l'était vraiment."


const text2 = "L'examen rapide de quelques cas observés chez les animaux domestiques nous permettra d'établie la possibilité ou même la probabilité de la transmission par hérédité des variations de l'instinct à l'état de nature. Nous pourrons apprécier, en même temps, le rôle que l'habitude et la selection des variations dites spontanées ont joué dnas les modifications qu'ont éprouvées les aptitdes mentales de nos animaux domestiques. On sait combien ils varient sous ce rapport. Certains chats, par exemple, attaquent naturellement des rats, d'autres se jettent sur les souris, et ces caractères sont héréditaires. Un chat, selon M. Saint-John, rapportait toujours à la maison du gibier à plumes, un autre des lièvres et des lapins;un troisième chassait dans les terrains marécageux et attrapait presque chaque nuit quelque bécassine. On pourrait citer un grand nombre de cas curieux et authentiques indiquant diverses nuances de caractère et de goût"


let counter = new Array(256).fill(0);
let sortCounter = [];

//intialise sortCounter
for(i=0;i<256;i++){
	sortCounter[i] = {
		description: "",
		asciiIndex: 0,
		value: 0,
	}
};

console.log("text.length:" + text.length);

//counter the number of stroke for each letter. The index of counter is the decimal number of the ascii character
for(j=0;j<text.length;j++){
   counter[text.charCodeAt(j)]++;
};

//Calculate the sum of the counter to see if it's the same as the text length
let keyCount = 0;

for(i=0;i<counter.length;i++){
	if(counter[i]>0){
		keyCount = keyCount + counter[i];
	};
};

if(keyCount != text.length){
	console.log("ERROR: Count of key is not equal to the text length");
}



// Sort counter in a new array: sortCounter
let counterBis = [...counter];
let indexOfMax = 0;

for(j=0;j<256;j++){
	for(i=1;i<256;i++){
		if(counterBis[i] > sortCounter[j].value){
			indexOfMax = i;
			sortCounter[j].description = String.fromCharCode(i);
			sortCounter[j].value = counterBis[i];
			sortCounter[j].asciiIndex = indexOfMax;
		};
	};
	counterBis[indexOfMax] = 0 
}

//display the used key by order
//let index = 0;
//while(sortCounter[index].value > 0){
//	console.log("Position: " + index + " : " + sortCounter[index].description + " taper " + sortCounter[index].value + " fois");
//	index++;
//}



//  MOONLANDER :
//
//   |----|----|----|----|----|----|----|                                 |----|----|----|----|----|----|----|
//   | 0,0| 0,1| 0,2| 0,3| 0,4| 0,5| 0,6|                                 | 7,6| 7,5| 7,4| 7,3| 7,2| 7,1| 7,0|
//   |----|----|----|----|----|----|----|                                 |----|----|----|----|----|----|----|
//   | 1,0| 1,1| 1,2| 1,3| 1,4| 1,5| 1,6|                                 | 8,6| 8,5| 8,4| 8,3| 8,2| 8,1| 8,0|
//   |----|----|----|----|----|----|----|                                 |----|----|----|----|----|----|----|
//   | 2,0| 2,1| 2,2| 2,3| 2,4| 2,5| 2,6|                                 | 9,6| 9,5| 9,4| 9,3| 9,2| 9,1| 9,0|
//   |----|----|----|----|----|----|----|                                 |----|----|----|----|----|----|----|
//   | 3,0| 3,1| 3,2| 3,3| 3,4| 3,5|                                           |10,5|10,4|10,3|10,2|10,1|10,0|
//   |----|----|----|----|----|----|                                           |----|----|----|----|----|----|
//   | 4,0| 4,1| 4,2| 4,3| 4,4|                                                     |11,4|11,3|11,2|11,1|11,0|
//   |----|----|----|----|----|                                                     |----|----|----|----|----|
//                                    |------|---                 ---|------|
//                                    |  6,7 |    -             -    | 13,7 |
//                                    |------| 5,7 |           | 12,7|------|
//                                    |  6,8 |    -             -    | 13,8 |
//                                    |------|---                 ---|------|
//                                    |  6,9 |                       | 13,9 |
//                                    |------|                       |------|
//
// Finger by key:
//
// 0 = LL = Little finger Left, 1 = RL = Ring finger Left, 2 = ML = Middle finger Left, 3 = IL = Index finger Left, 4 = TL = Thumb Left
// 5 = LR = Little finger Right, 6 = RR = Ring finger Right, 7 = MR = Middle finger Right, 8 = IR = Index finger Right, 9 = TR = Thumb Right
//
// let fingerLayout = [
//		[0,0,1,2,3,3,3,99,99,99], >
//		[0,0,1,2,3,3,3,99,99,99], > >
//		[0,0,1,2,3,3,3,99,99,99], > > >
//		[0,0,1,2,3,3,99,99,99,99], > > > > left hand
//		[0,0,1,2,3,99,99,99,99,99], > > >
//		[99,99,99,99,99,99,99,4,99,99], > >
//		[99,99,99,99,99,99,99,4,4,4], >
//		[5,5,6,7,8,8,8,99,99,99], >
//		[5,5,6,7,8,8,8,99,99,99], > >
//		[5,5,6,7,8,8,8,99,99,99], > > >
//		[5,5,6,7,8,8,99,99,99,99], > > > > right hand
//		[5,5,6,7,8,99,99,99,99,99], > > >
//		[99,99,99,99,99,99,99,9,99,99], > >
//		[99,99,99,99,99,99,99,9,9,9], >
//	];   |     |    |    |    |    |    |    |    |    |  
//	     \     /    |    |    \    |    /    \    |    /
//	      \   /     |    |     \   |   /      \   |   /
//        \ /      |    |      \  |  /        \  |  /
//      Little   ring  middle   index         thumb
//
//      this structure is design to facilitate the calcul of 'roll'

let fingerLayout = [
	[0,0,1,2,3,3,3,99,99,99],
	[0,0,1,2,3,3,3,99,99,99],
	[0,0,1,2,3,3,3,99,99,99],
	[0,0,1,2,3,3,99,99,99,99],
	[0,0,1,2,3,99,99,99,99,99],
	[99,99,99,99,99,99,99,4,99,99],
	[99,99,99,99,99,99,99,4,4,4],
	[5,5,6,7,8,8,8,99,99,99],
	[5,5,6,7,8,8,8,99,99,99],
	[5,5,6,7,8,8,8,99,99,99],
	[5,5,6,7,8,8,99,99,99,99],
	[5,5,6,7,8,99,99,99,99,99],
	[99,99,99,99,99,99,99,9,99,99],
	[99,99,99,99,99,99,99,9,9,9],
];

//Create a 'weight' for each key taht represente the 'distance' from the home postion. this will able to calculate the travel distance of fingers for a text.

let weightLayout = [
	[3,2,2,2,2,3,4,99,99,99],
	[2,1,1,1,1,2,3,99,99,99],
	[1,0,0,0,0,1,2,99,99,99],
	[2,1,1,1,1,2,99,99,99,99],
	[3,2,2,2,2,99,99,99,99,99],
	[99,99,99,99,99,99,99,2,99,99],
	[99,99,99,99,99,99,99,0,1,2],
	[3,2,2,2,2,3,4,99,99,99],
	[2,1,1,1,1,2,3,99,99,99],
	[1,0,0,0,0,1,2,99,99,99],
	[2,1,1,1,1,2,99,99,99,99],
	[3,2,2,2,2,99,99,99,99,99],
	[99,99,99,99,99,99,99,2,99,99],
	[99,99,99,99,99,99,99,0,1,2],
];

// fingerHomePosition: The index of the array correspond to the finger number. That array define the position where the finger are by default, the 'home' position.

let fingerHomePosition=[
   [2,1],
   [2,2],
   [2,3],
   [2,4],
   [6,7],
   [9,1],
   [9,2],
   [9,3],
   [9,4],
   [13,7],
];

let firstLayer = [
	["A","B","C","D","E","F","G","XX","XX","XX"],
	["H","I","J","K","L","M","N","XX","XX","XX"],
	["O","P","Q","R","S","T","U","XX","XX","XX"],
	["V","W","X","Y","Z",".","XX","XX","XX","XX"],
	["é","à","è","'","\"","XX","XX","XX","XX","XX"],
	["XX","XX","XX","XX","XX","XX","XX","1","XX","XX"],
	["XX","XX","XX","XX","XX","XX","XX"," ","2","3"],
	["a","b","c","d","e","f","g","XX","XX","XX"],
	["h","i","j","k","l","m","n","XX","XX","XX"],
	["o","p","q","r","s","t","u","XX","XX","XX"],
	["v","w","x","y","z","+","XX","XX","XX","XX"],
	["û","ô","î","â","ê","XX","XX","XX","XX","XX"],
	["XX","XX","XX","XX","XX","XX","XX","4","XX","XX"],
	["XX","XX","XX","XX","XX","XX","XX","-",";",","],
];

// *********
// find if there are char that don't have assigned key
// *********

let x = 0;
let charMapped = new Array(256).fill(0);

// create a array to summarize all assigned key of the different layer
for(j=0;j<10;j++){
	for(i=0;i<14;i++){
		charMapped[firstLayer[i][j].charCodeAt(0)]=1;
	}
}


while(sortCounter[x].value > 0){
	if(charMapped[sortCounter[x].asciiIndex] == 0){
		console.log("WARNING: the character " + String.fromCharCode(sortCounter[x].asciiIndex) + " is not mapped in the keyboard layout");
	}
	x++;
}




const NUMBER_OF_KEY = 72;

function makeKey(keyID, fingerLayoutRow, fingerLayoutColumn, firstLayerAsciiValue, finger, numberOfKeystroke){
	return {
		keyID: keyID,
		firstLayerAsciiValue: firstLayerAsciiValue,
		fingerLayoutColumn: fingerLayoutColumn,
		fingerLayoutRow: fingerLayoutRow,
		finger: finger,
		numberOfKeystroke: numberOfKeystroke,
	}
};

let IDCounter = 0;
let layout = [];

for(j=0;j<10;j++){
	for(i=0;i<14;i++){
		if(firstLayer[i][j] != "XX"){
			layout[IDCounter] = makeKey(IDCounter, i, j, firstLayer[i][j], fingerLayout[i][j], counter[firstLayer[i][j].charCodeAt(0)]);
			IDCounter++;
			};
	};	
};

//control the right length of the layout
if(layout.length != NUMBER_OF_KEY){
	console.log("ERROR: Layout length is not equal to NUMBER_OF_KEY");
};

// ********
// calculate finger usage
// ********

let strokeByFinger = new Array(10).fill(0);

layout.forEach(element => {
	strokeByFinger[element.finger] += element.numberOfKeystroke;
});

let percentagePerFinger = new Array(10).fill(0);
for(i=0;i<percentagePerFinger.length;i++){
	percentagePerFinger[i] = Math.round(100*100*(strokeByFinger[i])/text.length)/100;
}

let leftHandPercentage = percentagePerFinger[0]+percentagePerFinger[1]+percentagePerFinger[2]+percentagePerFinger[3]+percentagePerFinger[4];
let rightHandPercentage = percentagePerFinger[5]+percentagePerFinger[6]+percentagePerFinger[7]+percentagePerFinger[8]+percentagePerFinger[9];

console.log("**** Finger usage ****");
console.log("Left hand: " + leftHandPercentage + "%");
console.log("Right hand: " + rightHandPercentage + "%");
console.log("Left little finger : " + percentagePerFinger[0] + "%");
console.log("Left ring finger : " + percentagePerFinger[1] + "%");
console.log("Left middle finger : " + percentagePerFinger[2] + "%");
console.log("Left index finger : " + percentagePerFinger[3] + "%");
console.log("Left thumb finger : " + percentagePerFinger[4] + "%");
console.log("Right little finger : " + percentagePerFinger[5] + "%");
console.log("Right ring finger : " + percentagePerFinger[6] + "%");
console.log("Right middle finger : " + percentagePerFinger[7] + "%");
console.log("Right index finger : " + percentagePerFinger[8] + "%");
console.log("Right thumb finger : " + percentagePerFinger[9] + "%");

// ******
// Calculate finger travel
// ******

console.log("**** Finger Travel ****");

let position = 0;

let travelDistance = 0;
let totalTravelDistance = 0;


let previousLetterCode = text.charCodeAt(0);
let actualLetterCode = text.charCodeAt(0);
let previousLetterCoordinate = [0,0];
let previousFinger = 0;
let actualLetterCoordinate = [0,0];
let actualFinger = 0;


// 1 --- find the coordinate and the finger of the key
actualLetterCode = text.charCodeAt(position);

// asciiCoordinate is an array where the index correspond to the ascii decimal of a char. It define the coordinate of the key for each ascii value

let asciiCoordinate = new Array(255);
let asciiFinger = new Array(255);

layout.forEach(element => {
   asciiCoordinate[element.firstLayerAsciiValue.charCodeAt(position)] = [element.fingerLayoutRow,element.fingerLayoutColumn];
   asciiFinger[element.firstLayerAsciiValue.charCodeAt(position)] = element.finger;
});

actualLetterCoordinate = asciiCoordinate[actualLetterCode];
actualFinger = asciiFinger[actualLetterCode];

// 2 --- calculate the distance between actual and previous key
// For the first key, all finger are in home position, the travel distance is directly the weight.

travelDistance = weightLayout[actualLetterCoordinate[0]][actualLetterCoordinate[1]];

// 3 --- add the distance to the totalTravelDistance

totalTravelDistance += travelDistance;

// 4 --- Loop that for all the text

for(position=1;position<text.length;position++){
   //Pass the information from the actual letter to the previous letter
   previousLetterCode = actualLetterCode;
   previousLetterCoordinate = actualLetterCoordinate;
   previousFinger = actualFinger;
   //console.log("The previous letter code is: " + previousLetterCode + " ,coordinate: " + previousLetterCoordinate + " ,finger: " + previousFinger);
   //Find the new actual letter data
   actualLetterCode = text.charCodeAt(position);
   actualLetterCoordinate = asciiCoordinate[actualLetterCode];
   actualFinger = asciiFinger[actualLetterCode];
   //console.log("The actual letter code is: " + actualLetterCode + " ,coordinate: " + actualLetterCoordinate + " ,finger: " + actualFinger);
   //Calculate the travel distance
   //travel distance calculus depends if the previous and the actual letter use the same finger or not
   let isDifferentFinger = Boolean(actualFinger - previousFinger);
   travelDistance = isDifferentFinger*weightLayout[actualLetterCoordinate[0]][actualLetterCoordinate[1]] + !isDifferentFinger*(Math.abs(actualLetterCoordinate[0]-previousLetterCoordinate[0])+Math.abs(actualLetterCoordinate[1]-previousLetterCoordinate[1]));
   totalTravelDistance += travelDistance;
};

console.log("Total travel distance: " + totalTravelDistance);

