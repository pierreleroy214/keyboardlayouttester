# keyboardLayoutTester

## GOAL

Web app for testing moonlander keyboard layout for different type of input (french, english, javascript, C, email...)



## PHILOSPHY

- minimize the travel distance
- if 'roll' then travel 'bonus'
- about the same use for each finger

Additionnal need for personnal use:
- all numbers for left hand (to use one half and the mouse) 


## HOW TO TEST

number of press for each key => color scale for each key to make a visual of the keyboard
repartition of finger use
measure the fingers travel

Interresting links:
VIDEO: Using AI to create the perfect keyboard https://www.youtube.com/watch?vEOaPb9wrgDY

## INTERFACE
Web app to:
- Visualise layout
   [x] create a keyboard representation
   [ ] Link the keyoard representation to the layout
- Create layout
- Create content to test
- Test the layout
- Visualise results
- Understand the testing method

PS: I already apologize for my english, don't hesitate to contact me if you see any mistake in that repo. Thank you,
